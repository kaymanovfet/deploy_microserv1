#!/bin/bash

# Обновляем пакетный менеджер apt
echo --Step 1 install apt-get--
sudo apt-get update
sudo apt-get -y upgrade

echo --Step 2 python2 to python 3--
sudo ln -sfn /usr/bin/python3 /usr/bin/python

# Устанавливаем python pip
echo --Step 3 install python-pip--
sudo apt-get install -y python-pip

# Устанавливаем ansible
echo --Step 4 install ansible--
sudo apt-get install -y ansible
