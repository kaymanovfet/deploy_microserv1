# Configure the Azure provider
 terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
 }

 provider "azurerm" {
   features {}
 }

 resource  "azurerm_resource_group" "NC-RG"  {
     name = "NC_RG"
     location = "northeurope"
 }

 resource "azurerm_virtual_network" "task-vnet" {
     name = "taskvnet" 
     address_space = ["10.0.0.0/16"]
     location = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name
 }

 resource "azurerm_subnet" "subnet-NC" {
     name = "subnet_NC"
     resource_group_name = azurerm_resource_group.NC-RG.name
     virtual_network_name = azurerm_virtual_network.task-vnet.name
     address_prefixes = ["10.0.1.0/24"]
 }

 resource "azurerm_public_ip" "vm1publicip" {
     name = "pip1"
     location = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name
     allocation_method = "Dynamic"
     sku = "Basic"
 }

 resource "azurerm_public_ip" "vm2publicip" {
     name = "pip2"
     location = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name
     allocation_method = "Dynamic"
     sku = "Basic"
 }

 resource "azurerm_network_interface" "myvm1nic" {
     name = "myvm1-nic"
     location  = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name

     ip_configuration {
         name = "ipconfig1"
         subnet_id = azurerm_subnet.subnet-NC.id
         private_ip_address_allocation = "Dynamic"
         public_ip_address_id = azurerm_public_ip.vm1publicip.id
     }
 }

 resource "azurerm_network_interface" "myvm2nic" {
     name = "myvm2-nic"
     location = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name

     ip_configuration {
         name = "ipconfig2"
         subnet_id = azurerm_subnet.subnet-NC.id
         private_ip_address_allocation = "Dynamic"
         public_ip_address_id = azurerm_public_ip.vm2publicip.id
     }
 }

 resource "azurerm_linux_virtual_machine" "ex1" {
     name                            = "vm1"
     location                        = azurerm_resource_group.NC-RG.location
     resource_group_name             = azurerm_resource_group.NC-RG.name
     network_interface_ids           = [ azurerm_network_interface.myvm1nic.id ]
     size                            = "Standard_B1s"
     disable_password_authentication = false
     admin_username                  = "nick1"
     admin_password                  = "Password123"

     source_image_reference {
         publisher = "Canonical"
         offer     = "UbuntuServer"
         sku       = "18.04-LTS"
         version   = "latest"
     }

     os_disk {
         caching              = "ReadWrite"
         storage_account_type = "Standard_LRS"
     }
 }

 resource "azurerm_linux_virtual_machine" "ex2" {
     name                            = "vm2"
     location                        = azurerm_resource_group.NC-RG.location
     resource_group_name             = azurerm_resource_group.NC-RG.name
     network_interface_ids           = [ azurerm_network_interface.myvm2nic.id ]
     size                            = "Standard_B2ms"
     disable_password_authentication = false
     admin_username                  = "nick2"
     admin_password                  = "Password123"

     source_image_reference {
         publisher = "Canonical"
         offer     = "UbuntuServer"
         sku       = "18.04-LTS"
         version   = "latest"
     }

     os_disk {
         caching              = "ReadWrite"
         storage_account_type = "Standard_LRS"
     }
 }

 resource "azurerm_network_security_group" "ex1_2" {
     name                = "Security_group_vm"
     location            = azurerm_resource_group.NC-RG.location
     resource_group_name = azurerm_resource_group.NC-RG.name

     security_rule {
         name                       = "test1"
         priority                   = 100
         direction                  = "Inbound"
         access                     = "Allow"
         protocol                   = "Tcp"
         source_port_range          = "*"
         destination_port_range     = "22"
         source_address_prefix      = "*"
         destination_address_prefix = "*"
     }

     tags = {
         environment = "Production"
     }
 } 
